---
title: Sophie is adorable
date: 2019-04-05
crypto: 13SD9NRycuydJVT3LSFi27fMV3vcuLZoJN
---

Sophie has two ways to get to my lap  
One is painful, the other surprising.  
She chooses surprising more often than painful.  
But today, she decided to climb my leg  
inch by inch, with her sharp claws on my pants.  
I like the jump better, it startles me,  
but at least it does not hurt.  

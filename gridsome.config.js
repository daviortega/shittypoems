module.exports = {
  siteName: `Shitty Poems`,
  titleTemplate: `%s`,

  transformers: {
    remark: {
      parse (source) {
        const { data: fields, content, excerpt } = parse(source)
    
        // if no title was found by gray-matter,
        // try to find the first one in the content
        if (!fields.title) {
          const title = content.trim().match(/^#+\s+(.*)/)
          if (title) fields.title = title[1]
        }
    
        return {
          title: fields.title,
          slug: fields.slug,
          path: fields.path,
          date: fields.date,
          crypto: fields.crypto,
          content,
          excerpt,
          fields
        }
      }
    }
  },

  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'blog/*.md',
        typeName: 'BlogPost',
        route: '/:slug'
      }
    }
  ]
}
